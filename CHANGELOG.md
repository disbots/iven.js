# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [UNRELEASED]
#### Added
- Help Command that displays all initialized commands with their syntax and usage
- Avatar command sending links to users avatars
- `CONTRIBUTING.md`
- Automated writing of a log file to keep track of executed commands
- Added poke command

#### Changed
- Export of the Commands in `index.js` for the Help Command
- `README.MD` because it wasn't really helpful before

#### Removed
- Glitches :)

## [1.0.0] - 2019-01-21
#### Added
- Login functionality
- Automated command module loading + Commandhandler
- Added ping command for testing purposes

#### Changed
- Structure of changelog updated for better readability
- License was updated

#### Removed
- Bugs :)