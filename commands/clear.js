/*
 * Copyright (c) 2019. Iven Beck
 * You are free to use this code if you give it as a source.
 */
let botconfig = require("../botconfig.json");

module.exports.run = async (bot, msg, args) => {
    if (!msg.member.hasPermission("MANAGE_MESSAGES")) return msg.reply("you have got insufficient permissions\nTry using `" + botconfig.prefix + "help [command]`");
    let deletecount = parseInt(args[0], 10);
    if (!deletecount || deletecount < 2 || deletecount > 100) {
        return msg.reply("Please provide a number between 2 and 100 for the number of messages to delete")
    }
    let fetched = await msg.channel.fetchMessages({limit: deletecount});
    msg.channel.bulkDelete(fetched).catch(e => msg.reply(`An error occured\n\`[${e}]\``));
    msg.channel.send(`Deleted ${deletecount} messages.`).then(m => m.delete(5000))
};

module.exports.help = {
    name: "clear",
    description: "Deletes a defined amount of messages from a channel",
    perms: "MANAGE_MESSAGES",
    syntax: "clear {amount}"
};
