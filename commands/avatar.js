/*
 * Copyright (c) 2019. Iven Beck
 * You are free to use this code if you give it as a source.
 */

module.exports.run = (bot, msg, args) => {
    if (args.length === 0) {
        msg.channel.send(msg.author.avatarURL)
    }
    let user;
    if (args.length === 1) {
        try {
            user = msg.mentions.users.first();
            if (user.avatarURL) msg.channel.send(user.avatarURL);
            else if (user.defaultAvatarURL) msg.channel.send(user.defaultAvatarURL)
        } catch (e) {
            msg.channel.send("Query Error: `Invalid User`");
        }
    }
};

module.exports.help = {
    name: "avatar",
    description: "Gets an users avatar and sends it",
    perms: "",
    syntax: "avatar [user]"
};