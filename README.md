# iven.js

A modular, interactive Discord Bot realized in NodeJS.

[![](https://img.shields.io/discord/541299043205120011.svg?colorB=blue&label=Discord&style=flat&link=https://discord.gg/TUtXJMn&link=https://discord.gg/TUtXJMn)](https://discord.gg/TUtXJMn)
[![](https://img.shields.io/gitlab/pipeline/disbots/iven.js.svg?style=popout)]()

## Getting Started

These instructions will get you a copy of the project up and running on your local machine or basically
any other machine capable of running NodeJS.

### Prerequisites

```
Node.js 8.12.0 or higher
```

### Installing

A step by step series of examples that tell you how to get the bot up and running

Clone git repository

```
$ git clone https://gitlab.com/disbots/iven.js.git
```

Install dependencies

```
$ npm install
```

Change token and prefix in botconfig.json

```
$ vi botconfig.json
```

Run the bot

```
$ node index.js
```

After having the bot added to a guild, try sending $ping ($ ressembles your prefix) and the bot should reply.
If that however doesn't work, please contact me via discord `@Iven#8562`

## Built With

* [NodeJS](https://nodejs.org/) - The framework used to realize the bot
* [Discord.JS](https://discord.js.org/#/) - Discord API implementation

## Versioning

This project uses [SemVer](http://semver.org/) for versioning. For the versions available, see the [releases on this repository](https://gitlab.com/disbots/iven.js/releases). 

## Contributing
See [CONTRIBUTING](CONTRINUTING.md) file for details

## Authors

* **Iven Beck** - *Maintainer* - [dev.ib](https://gitlab.com/dev.ib)

## License

See [LICENSE](LICENSE.md) file for details

## Acknowledgments

* If my code is bad, don't be sad (╯°□°）╯︵ ┻━┻
